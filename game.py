name = input("Hi! What's your name? ")

from random import randint

# Guess 5 times

for guess_number in range(5):

    month = randint(1,12)
    year = randint(1924,2004)
    print("Guess", guess_number + 1, ":", name, "were you born in ", month, "/", year)
    response = input("yes or no?")
    if response == "yes":
        print("I knew it!")
        break
    elif guess_number == 4 and response == "no":
        print ("I have better things to do! Goodbye!")
        break
    else:
        print("Drat! Lemme try again!")
